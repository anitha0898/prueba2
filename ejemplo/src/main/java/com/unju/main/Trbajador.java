package com.unju.main;
import java.util.Date;
public class Trbajador {


		//atributos
	      private String nombre;
	      private String puesto;
	      private String direccion;
	      private String telefono;
	      private Date fechaNacimieno;
	      private Date fechaContrato;
	      //contructores
	      public Trbajador() {
	    	  
	      }
	      
	      public Trbajador(String nombre) {
			this.nombre = nombre;
		}
	      
	    //metods get y set para poder accer desde aki a los atributos de tipo private usamos encapsulamiento
		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getPuesto() {
			return puesto;
		}

		public void setPuesto(String puesto) {
			this.puesto = puesto;
		}

		public String getDireccion() {
			return direccion;
		}

		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}

		public String getTelefono() {
			return telefono;
		}

		public void setTelefono(String telefono) {
			this.telefono = telefono;
		}

		public Date getFechaNacimieno() {
			return fechaNacimieno;
		}

		public void setFechaNacimieno(Date fechaNacimieno) {
			this.fechaNacimieno = fechaNacimieno;
		}

		public Date getFechaContrato() {
			return fechaContrato;
		}

		public void setFechaContrato(Date fechaContrato) {
			this.fechaContrato = fechaContrato;
		}

		//metodos
		public String toString() {
			return nombre;
		}
	      
	      
	      
	
}
